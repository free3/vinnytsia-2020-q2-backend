import { EventEmitter } from 'events';

const Stream = new EventEmitter();
Stream.setMaxListeners(50);

export default Stream;
