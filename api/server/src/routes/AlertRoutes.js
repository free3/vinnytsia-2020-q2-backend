import { Router } from 'express';
import AlertCreator from '../services/AlertService';

const router = Router();

router.route('/').get(AlertCreator.generateSensorAlerts);
router.route('/:id').get(AlertCreator.generateSensorAlert);

export default router;
