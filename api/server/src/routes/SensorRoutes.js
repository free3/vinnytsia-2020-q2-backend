import { Router } from 'express';
import SensorController from '../controllers/SensorController';
import auth from '../middleware/auth';

const router = Router();

router
  .route('/')
  .get(auth, SensorController.getAllsensors)
  .post(auth, SensorController.addSensor)
  .put(auth, SensorController.updateAllSensor);

router
  .route('/:id')
  .get(auth, SensorController.getHouseSensors)
  .put(auth, SensorController.updateSensor)
  .delete(auth, SensorController.deleteSensor);

export default router;
