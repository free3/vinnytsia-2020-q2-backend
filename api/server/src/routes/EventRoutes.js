import { Router } from 'express';
import SSEController from '../controllers/ServerEventController';
import EventController from '../controllers/EventController';
import auth from '../middleware/auth';

const router = Router();

router.route('/').get(SSEController.connect);
router.route('/:id').get(auth, EventController.getHouseEvents);

export default router;
