import { Router } from 'express';
import FeedbackController from '../controllers/FeedbackController';
import auth from '../middleware/auth';

const router = Router();

router.route('/').post(auth, FeedbackController.sendFeedback);

export default router;
