import { Router } from 'express';
import HouseController from '../controllers/HouseController';
import auth from '../middleware/auth';

const router = Router();

router
  .route('/')
  .get(auth, HouseController.getHouse)
  .post(auth, HouseController.addHouse);

router.route('/:id').put(auth,HouseController.updateHouse);

export default router;
