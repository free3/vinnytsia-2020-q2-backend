import { Router } from 'express';
import ProfilesController from '../controllers/ProfileController';
import auth from '../middleware/auth';

const router = Router();

router.route('/').post(auth, ProfilesController.addProfile);

router
  .route('/:id')
  .get(auth, ProfilesController.getHouseProfiles)
  .delete(auth, ProfilesController.deleteProfile);

export default router;
