import { Router } from 'express';
import DataController from '../controllers/DataController';
import auth from '../middleware/auth';

const router = Router();

router.route('/').get(auth, DataController.getData);

export default router;
