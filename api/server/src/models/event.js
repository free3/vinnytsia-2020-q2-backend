module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define(
    'Event',
    {
      type: DataTypes.STRING,
      action: DataTypes.STRING,
      userName: DataTypes.STRING,
      userImage: DataTypes.STRING,
    },
    {},
  );
  Event.associate = models => {
    // associations can be defined here
    Event.belongsTo(models.House);
  };
  return Event;
};
