module.exports = (sequelize, DataTypes) => {
  const House = sequelize.define(
    'House',
    {
      housePlan: DataTypes.TEXT,
      securityStatus: DataTypes.BOOLEAN,
      currentProfile: DataTypes.INTEGER,
    },
    {},
  );
  House.associate = models => {
    // associations can be defined here
    House.hasMany(models.Sensor);
    House.hasMany(models.Profile);
    House.hasMany(models.Event);
  };
  return House;
};
