module.exports = (sequelize, DataTypes) => {
  const Sensor = sequelize.define(
    'Sensor',
    {
      room: DataTypes.STRING,
      x: DataTypes.INTEGER,
      y: DataTypes.INTEGER,
      alertDate: DataTypes.STRING,
      alert: DataTypes.BOOLEAN,
      isArmed: DataTypes.BOOLEAN,
      status: DataTypes.BOOLEAN,
      nightMode: DataTypes.BOOLEAN,
    },
    {},
  );
  Sensor.associate = models => {
    Sensor.belongsTo(models.House);
  };
  return Sensor;
};
