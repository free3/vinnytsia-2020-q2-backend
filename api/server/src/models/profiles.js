module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define(
    'Profile',
    {
      profileName: DataTypes.STRING,
      sunday: DataTypes.BOOLEAN,
      monday: DataTypes.BOOLEAN,
      tuesday: DataTypes.BOOLEAN,
      wednesday: DataTypes.BOOLEAN,
      thursday: DataTypes.BOOLEAN,
      friday: DataTypes.BOOLEAN,
      saturday: DataTypes.BOOLEAN,
      timeFrom: DataTypes.STRING,
      timeTo: DataTypes.STRING,
    },
    {},
  );
  Profile.associate = models => {
    // associations can be defined here
    Profile.belongsTo(models.House);
  };
  return Profile;
};
