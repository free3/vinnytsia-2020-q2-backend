import { CronJob } from 'cron';
import map from 'lodash/map';
import compact from 'lodash/compact';
import flatten from 'lodash/flatten';
import split from 'lodash/split';
import { DateTime } from 'luxon';
import ProfileService from './ProfileService';
import SensorService from './SensorService';
import HouseService from './HouseService';
import Stream from '../utils/EventEmitter';

const daysOfWeek = [
  ['sunday', 0],
  ['monday', 1],
  ['tuesday', 2],
  ['wednesday', 3],
  ['thursday', 4],
  ['friday', 5],
  ['saturday', 6],
];

async function createCronJobs(time, isArmed, day, HouseId, timeTo) {
  const timeNow = DateTime.local()
    .setZone('Europe/Kiev')
    .toMillis();
  if (
    isArmed &&
    timeNow >=
      DateTime.fromISO(time.join(':'))
        .setZone('Europe/Kiev')
        .toMillis() &&
    timeNow < DateTime.fromISO(timeTo.join(':')).toMillis()
  ) {
    console.log('start');
    await SensorService.updateAllSensors(
      HouseId,
      {
        isArmed,
      },
      { HouseId: Number(HouseId), status: true },
    );
    Stream.emit('newEvent', JSON.stringify({ type: 'default' }));
  }
  return new CronJob(
    `00 ${time[1]} ${time[0]} * * ${day[1]}`,
    async () => {
      await SensorService.updateAllSensors(
        HouseId,
        {
          isArmed,
        },
        { HouseId: Number(HouseId), status: true },
      );
      Stream.emit('newEvent', JSON.stringify({ type: 'default' }));
    },
    null,
    true,
    'Europe/Kiev',
  );
}

class JobsService {
  static async createJobs() {
    let jobs;
    try {
      const house = await HouseService.getHouse();
      if (house) {
        const currentProfile = await ProfileService.getProfile(
          house.currentProfile,
        );
        const sensors = await SensorService.getHouseSensors(house.id);
        if (currentProfile && sensors.length > 0 && house.securityStatus) {
          const timeFrom = split(currentProfile.timeFrom, ':');
          const timeTo = split(currentProfile.timeTo, ':');
          jobs = flatten(
            compact(
              map(daysOfWeek, day => {
                if (currentProfile[day[0]]) {
                  console.log(currentProfile[day[0]]);
                  return [
                    createCronJobs(timeFrom, true, day, house.id, timeTo),
                    createCronJobs(timeTo, false, day, house.id),
                  ];
                }
              }),
            ),
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
    // console.log(jobs, 'jobs creator');
    return jobs;
  }
}

export default JobsService;
