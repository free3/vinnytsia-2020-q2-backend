import { DateTime } from 'luxon';
import random from 'lodash/random';
import { CronJob } from 'cron';
import HouseService from './HouseService';
import SensorService from './SensorService';
import EventService from './EventService';
import Stream from '../utils/EventEmitter';

class AlertCreator {
  static async generateSensorAlert(request, response) {
    response.end('work1');
    // const time = DateTime.local()
    //   .setZone('Europe/Kiev')
    //   .toFormat('HH');
    // console.log(time);
  }

  static async generateSensorAlerts(requset, response) {
    const House = await HouseService.getHouse();
    const time = DateTime.local().setZone('Europe/Kiev');
    console.log('work alertService');
    response.send('work');

    if (House) {
      console.log(House.securityStatus, House.id, 'stats');
      if (House.securityStatus) {
        const job = new CronJob(
          '*/5 * * * * *',
          async () => {
            let sensors = await SensorService.getHouseSensors(House.id);
            if (sensors.length > 0) {
              const randomSensor = random(0, sensors.length - 1);
              console.log(sensors.length, randomSensor, 'random');
              const sensor = sensors[randomSensor];
              if (
                (sensor.isArmed && sensor.status && !sensor.alert) ||
                (sensor.status &&
                  sensor.nightMode &&
                  time.hour >= 23 &&
                  time.hour <= 6 &&
                  !sensor.alert)
              ) {
                const newSensor = {
                  HouseId: sensor.HouseId,
                  alert: true,
                  alertDate: DateTime.local()
                    .setZone('Europe/Kiev')
                    .toFormat('x'),
                };
                console.log('sensor update', sensor.id);
                sensors = await SensorService.updateSensor(
                  sensor.id,
                  newSensor,
                );
                const newEvent = {
                  action: `Sensor ${sensor.id} detected move in room: ${sensor.room}`,
                  type: 'sensor',
                };
                const event = await EventService.addEvent(newEvent);
                console.log('work');
                if (event) {
                  Stream.emit('newEvent', JSON.stringify(event));
                }
              }
            }
          },
          null,
          true,
        );
        const timeout = setTimeout(() => {
          job.stop();
          clearTimeout(timeout);
        }, 300000);
      }
    }
  }
}
export default AlertCreator;
