import database from '../models';

class SensorService {
  static getHouseSensors(id) {
    return database.Sensor.findAll({
      where: { HouseId: Number(id) },
    });
  }

  static getAllSensors() {
    return database.findAll();
  }

  static async addSensor(id) {
    await database.Sensor.create({ HouseId: Number(id) });
    return this.getHouseSensors(id);
  }

  static async updateSensor(id, updateSensor) {
    const sensorToUpdate = await database.Sensor.findOne({
      where: { id: Number(id) },
    });
    if (sensorToUpdate) {
      await database.Sensor.update(updateSensor, { where: { id: Number(id) } });
      return this.getHouseSensors(updateSensor.HouseId);
    }
    return null;
  }

  static async updateAllSensors(HouseId, updateSensors, condition) {
    const sensorToUpdate = await database.Sensor.findOne({
      where: { HouseId: Number(HouseId) },
    });
    if (sensorToUpdate) {
      await database.Sensor.update(updateSensors, {
        where: condition,
      });
      console.log('work sensor update');

      return this.getHouseSensors(HouseId);
    }
    return null;
  }

  static async deleteSensor(id) {
    const sensorToDelete = await database.Sensor.findOne({
      where: { id: Number(id) },
    });

    if (sensorToDelete) {
      await database.Sensor.destroy({
        where: { id: Number(id) },
      });
      return this.getHouseSensors(sensorToDelete.HouseId);
    }
    return null;
  }
}

export default SensorService;
