import database from '../models';

class ProfileService {
  static getHouseProfiles(id) {
    return database.Profile.findAll({ where: { HouseId: Number(id) } });
  }

  static getProfile(id) {
    return database.Profile.findOne({ where: { id: Number(id) } });
  }

  static addProfile(newProfile) {
    return database.Profile.create(newProfile);
  }

  static async deleteProfile(id) {
    const profileToDelete = await database.Profile.findOne({
      where: { id: Number(id) },
    });
    if (profileToDelete) {
      const deletedProfile = await database.Profile.destroy({
        where: { id: Number(id) },
      });
      return { deletedProfile, profileToDelete };
    }
    return null;
  }
}

export default ProfileService;
