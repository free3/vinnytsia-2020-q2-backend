import database from '../models';
import HouseService from './HouseService';

class EventService {
  static getHouseEvents(id) {
    return database.Event.findAll({
      where: { HouseId: Number(id) },
    });
  }

  static async addEvent(event) {
    if (!event) return null;
    const House = await HouseService.getHouse();
    if (House) {
      return database.Event.create({
        HouseId: House.id,
        ...event,
      });
    }
    return null;
  }
}
export default EventService;
