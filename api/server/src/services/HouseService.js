import database from '../models';

class HouseService {
  static async getHouse() {
    const House = await database.House.findOne();
    return House;
  }

  static async addHouse() {
    const house = await database.House.findOne();
    if (house) return house;
    return database.House.create();
  }

  static async updateHouse(id, updateHouse) {
    const houseToUpdate = await database.House.findOne({
      where: { id: Number(id) },
    });
    if (houseToUpdate) {
      await database.House.update(updateHouse, {
        where: { id: Number(id) },
      });
      const updatedHouse = await database.House.findOne({
        where: { id: Number(id) },
      });
      return updatedHouse;
    }
    return null;
  }
}

export default HouseService;
