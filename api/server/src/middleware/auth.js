import fetch from 'node-fetch';
import Util from '../utils/Utils';
import config from '../config/config';

const util = new Util();

export default async function auth(request, response, next) {
  const token = request.headers.authorization;
  if (!token) {
    util.setError(401, 'User not authorized');
    return util.send(response);
  }
  try {
    const data = await fetch(config.authUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });
    const user = await data.json();
    if (user.status === 'success') {
      request.user = user.data;
      console.log('auth success');
      return next();
    }
    util.setError(401, 'User not authorized');
    return util.send(response);
  } catch (error) {
    util.setError(401, error.message);
    return util.send(response);
  }
}
