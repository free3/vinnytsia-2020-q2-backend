import nodemailer from 'nodemailer';
import Util from '../utils/Utils';
import config from '../config/config';

const util = new Util();

class FeedbackController {
  static async sendFeedback(request, response) {
    const data = request.body;
    console.log(data);
    if (!data) {
      util.setError(400, 'No feedback data');
      return util.send(response);
    }
    console.log('message', config.mail, config.mailPassword);
    try {
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        // port: 587,
        // secure: false, // true for 465, false for other ports
        auth: {
          user: config.mail, // generated ethereal user
          pass: config.mailPassword, // generated ethereal password
        },
      });

      // send mail with defined transport object
      await transporter.sendMail(
        {
          from: data.userMail,
          subject: 'Feedback', 
          text: `From: ${data.userName}
        Message: ${data.userMessage}`, 
        },
        error => {
          console.log(error);
        },
      );
      util.setSuccess(200, 'message delivered house');
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default FeedbackController;
