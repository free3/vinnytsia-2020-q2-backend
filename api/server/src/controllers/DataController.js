import HouseService from '../services/HouseService';
import SensorService from '../services/SensorService';
import ProfileService from '../services/ProfileService';
import EventService from '../services/EventService';
import Util from '../utils/Utils';

const util = new Util();

class DataController {
  static async getData(request, response) {
    try {
      let house = await HouseService.getHouse();
      if (!house) {
        house = await HouseService.addHouse();
      }
      const events = await EventService.getHouseEvents(house.id);
      const profiles = await ProfileService.getHouseProfiles(house.id);
      const sensors = await SensorService.getHouseSensors(house.id);
      const data = {
        house,
        events,
        profiles,
        sensors,
      };
      util.setSuccess(200, 'Found data', data);
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default DataController;
