import EventService from '../services/EventService';
import Util from '../utils/Utils';

const util = new Util();

class EventController {
  static async getHouseEvents(request, response) {
    const { id } = request.params;
    if (!Number(id)) {
      util.setError(400, 'No house id');
      return util.send(response);
    }
    try {
      const Events = await EventService.getHouseEvents(id);
      if (Events.length > 0) {
        util.setSuccess(200, 'Found events', Events);
      } else {
        util.setSuccess(200, `No events founded`);
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default EventController;
