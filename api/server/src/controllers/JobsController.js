import forEach from 'lodash/forEach';
import Stream from '../utils/EventEmitter';
import JobsService from '../services/JobsService';

class JobsController {
  static async jobsHandler() {
    const jobs = await JobsService.createJobs();
    console.log(jobs);
    if (jobs && jobs.length > 0) {
      Stream.on('job', async data => {
        if (data === 'destroy') {
          forEach(jobs, async job => {
            const newJob = await job;
            newJob.stop();
          });
        }
        if (data === 'start') {
          if (jobs && jobs.length > 0) {
            forEach(jobs, async job => {
              const newJob = await job;
              newJob.stop();
            });
          }
          JobsService.createJobs();
        }
      });
    }
    return null;
  }
}

export default JobsController;
