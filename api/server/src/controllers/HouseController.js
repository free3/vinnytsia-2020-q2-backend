import HouseService from '../services/HouseService';
import Util from '../utils/Utils';
import EventService from '../services/EventService';
import Stream from '../utils/EventEmitter';

const util = new Util();

class HouseController {
  static async getHouse(request, response) {
    try {
      const House = await HouseService.getHouse();

      if (!House) {
        util.setError(404, `Cannot find house`);
      } else {
        util.setSuccess(200, 'Found house', House);
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async addHouse(request, response) {
    try {
      const house = await HouseService.addHouse();
      util.setSuccess(201, 'success', house);
      return util.send(response);
    } catch (error) {
      util.setError(500, error.message);
      return util.send(response);
    }
  }

  static async updateHouse(request, response) {
    const { updatedData, action } = request.body;
    const { id } = request.params;
    const { userName, currentImage: userImage } = request.user;
    try {
      const updateHouse = await HouseService.updateHouse(id, updatedData);
      if (!updateHouse) {
        util.setError(404, `Cannot find house`);
      } else {
        util.setSuccess(200, 'House updated', updateHouse);
        console.log(updatedData);
        if (updatedData.currentProfile) {
          Stream.emit('job', 'start');
        } else if (updatedData.currentProfile === 0) {
          Stream.emit('job', 'destroy');
        }
        try {
          if (action) {
            const newEvent = {
              action,
              type: 'house',
              userName,
              userImage,
            };
            const event = await EventService.addEvent(newEvent);
            if (event) {
              console.log('house Event', new Date());
              Stream.emit('newEvent', JSON.stringify(event));
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default HouseController;
