import Stream from '../utils/EventEmitter';

class SSEControler {
  static connect(request, response) {
    response.setHeader('Cache-Control', 'no-cache');
    response.setHeader('Content-Type', 'text/event-stream');
    response.setHeader('Connection', 'keep-alive');
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.flushHeaders();
    response.write('retry: 5000');
    response.write('data: connected\n');
    function eventHandler(data) {
      console.log('event');
      response.write(`event: onEvent\ndata: ${data}\n\n`);
    }

    Stream.on('newEvent', eventHandler);
    request.on('close', () => {
      console.log('closed');
      Stream.removeListener('newEvent', eventHandler);
      response.end();
    });
  }
}

export default SSEControler;
