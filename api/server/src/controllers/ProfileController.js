import ProfileService from '../services/ProfileService';
import Util from '../utils/Utils';
import HouseService from '../services/HouseService';
import Stream from '../utils/EventEmitter';
import EventService from '../services/EventService';

const util = new Util();

class ProfileController {
  static async getHouseProfiles(request, response) {
    const { id } = request.params;
    if (!Number(id)) {
      util.setError(400, 'No house id');
      return util.send(response);
    }
    try {
      const allProfiles = await ProfileService.getHouseProfiles(id);
      if (allProfiles.length > 0) {
        util.setSuccess(200, 'Profiles retrieved', allProfiles);
      } else {
        util.setSuccess(200, 'No profile found');
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async addProfile(request, response) {
    const { HouseId, newProfile } = request.body;
    const { userName, currentImage: userImage } = request.user;
    if (
      !HouseId ||
      !newProfile.timeFrom ||
      !newProfile.timeTo ||
      !newProfile.profileName
    ) {
      util.setError(400, 'Please provide complete details');
      return util.send(response);
    }
    try {
      const createdProfile = await ProfileService.addProfile({
        HouseId,
        ...newProfile,
      });
      const newEvent = {
        action: `Profile ${newProfile.profileName} was added`,
        type: 'profile',
        userName,
        userImage,
      };
      const event = await EventService.addEvent(newEvent);
      if (event) {
        Stream.emit('newEvent', JSON.stringify(event));
      }
      util.setSuccess(201, 'Profile Added!', createdProfile);
      return util.send(response);
    } catch (error) {
      util.setError(500, error.message);
      return util.send(response);
    }
  }

  static async deleteProfile(request, response) {
    const { id } = request.params;
    const { userName, currentImage: userImage } = request.user;
    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(response);
    }
    try {
      const profile = await ProfileService.deleteProfile(id);
      if (profile.deletedProfile) {
        await HouseService.updateHouse(profile.profileToDelete.HouseId, {
          currentProfile: 0,
        });
        const newEvent = {
          action: `Profile ${profile.profileToDelete.profileName} deleted`,
          type: 'house',
          userName,
          userImage,
        };
        const event = await EventService.addEvent(newEvent);
        if (event) {
          Stream.emit('newEvent', JSON.stringify(event));
        }
        util.setSuccess(200, 'Profile deleted');
      } else {
        util.setError(404, `Profile cannot be found`);
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default ProfileController;
