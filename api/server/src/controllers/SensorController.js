import SensorService from '../services/SensorService';
import Util from '../utils/Utils';
import EventService from '../services/EventService';
import Stream from '../utils/EventEmitter';

const util = new Util();

class SensorController {
  static async getHouseSensors(request, response) {
    const { id } = request.params;
    if (!Number(id)) {
      util.setError(400, 'No house id');
      return util.send(response);
    }
    try {
      const allSensors = await SensorService.getHouseSensors(id);

      if (allSensors.length > 0) {
        util.setSuccess(200, 'Sensors retrieved', allSensors);
      } else {
        util.setSuccess(200, 'No sensor found');
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async getAllsensors(request, response) {
    try {
      const allSensors = await SensorService.getAllSensors();

      if (allSensors.length > 0) {
        util.setSuccess(200, 'Sensors retrieved', allSensors);
      } else {
        util.setSuccess(200, 'No sensor found');
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async addSensor(request, response) {
    const { HouseId, action } = request.body;
    const { userName, currentImage: userImage } = request.user;
    if (!HouseId) {
      util.setError(400, 'No house id');
      return util.send(response);
    }
    try {
      const createdSensor = await SensorService.addSensor(HouseId);
      util.setSuccess(201, 'Sensor Added!', createdSensor);
      try {
        if (action) {
          const newEvent = {
            action,
            type: 'sensor',
            userName,
            userImage,
          };
          const events = await EventService.addEvent(newEvent);
          console.log(!!events);
          if (events) {
            console.log('work', 2);
            Stream.emit('newEvent', JSON.stringify(events));
          }
        }
      } catch (error) {
        console.log(error);
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error.message);
      return util.send(response);
    }
  }

  static async updateSensor(request, response) {
    const { newSensor, action } = request.body;
    const { id } = request.params;
    const { userName, currentImage: userImage } = request.user;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(response);
    }
    try {
      const updatedSensors = await SensorService.updateSensor(id, newSensor);
      if (!updatedSensors) {
        util.setError(404, `Cannot find sensor with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Sensor updated', updatedSensors);
        try {
          if (action) {
            const newEvent = {
              action,
              type: 'sensor',
              userName,
              userImage,
            };
            const events = await EventService.addEvent(newEvent);
            if (events) {
              Stream.emit('newEvent', JSON.stringify(events));
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async updateAllSensor(request, response) {
    const { HouseId, newSensors, action } = request.body;
    const { userName, currentImage: userImage } = request.user;
    if (!HouseId || !newSensors) {
      util.setError(400, 'Bad request data');
      return util.send(response);
    }
    try {
      const updatedSensors = await SensorService.updateAllSensors(
        HouseId,
        newSensors,
        { HouseId: Number(HouseId) },
      );
      if (!updatedSensors) {
        util.setError(404, `Cannot find sensors}`);
      } else {
        util.setSuccess(200, 'Sensors updated', updatedSensors);
        try {
          if (action) {
            const newEvent = {
              action,
              type: 'sensor',
              userName,
              userImage,
            };
            const events = await EventService.addEvent(newEvent);
            if (events) {
              Stream.emit('newEvent', JSON.stringify(events));
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }

  static async deleteSensor(request, response) {
    const { id } = request.params;
    const { userName, currentImage: userImage } = request.user;
    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(response);
    }
    try {
      const updatedSensors = await SensorService.deleteSensor(id);
      if (updatedSensors) {
        util.setSuccess(200, 'Sensor updated', updatedSensors);
        try {
          const newEvent = {
            action: `Sensor ${id} deleted`,
            type: 'sensor',
            userName,
            userImage,
          };
          const event = await EventService.addEvent(newEvent);
          if (event) {
            Stream.emit('newEvent', JSON.stringify(event));
          }
        } catch (error) {
          console.log(error);
        }
      } else {
        util.setError(404, `Sensor with the id ${id} cannot be found`);
      }
      return util.send(response);
    } catch (error) {
      util.setError(500, error);
      return util.send(response);
    }
  }
}

export default SensorController;
