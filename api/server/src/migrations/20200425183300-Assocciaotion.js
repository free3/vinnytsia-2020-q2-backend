module.exports = {
  up: async (queryInterface, Sequelize) => {
    // House hasMany Sensors
    await queryInterface.addColumn('Sensors', 'HouseId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'Houses',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
    // House hasMany profiles
    await queryInterface.addColumn('Profiles', 'HouseId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'Houses',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
    await queryInterface.addColumn('Events', 'HouseId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'Houses',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
  },
  down: async queryInterface => {
    await queryInterface.removeColumn('Sensors', 'HouseId');
    await queryInterface.removeColumn('Events', 'HouseId');
    return queryInterface.removeColumn('Profiles', 'HouseId');
  },
};
