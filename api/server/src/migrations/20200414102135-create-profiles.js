module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Profiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      profileName: {
        type: Sequelize.STRING,
      },
      sunday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      monday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      tuesday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      wednesday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      thursday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      friday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      saturday: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      timeFrom: {
        type: Sequelize.STRING,
      },
      timeTo: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('Profiles');
  },
};
