module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Sensors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      room: {
        type: Sequelize.STRING,
        defaultValue: 'not specified',
      },
      x: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      y: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      alertDate: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
      },
      alert: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isArmed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      nightMode: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('Sensors');
  },
};
