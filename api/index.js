import config from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import HouseRoutes from './server/src/routes/HouseRoutes';
import ProfileRoutes from './server/src/routes/ProfileRoutes';
import SensorRoutes from './server/src/routes/SensorRoutes';
import ServerEventRoutes from './server/src/routes/EventRoutes';
import AlertRoutes from './server/src/routes/AlertRoutes';
import JobsController from './server/src/controllers/JobsController';
import DataRoutes from './server/src/routes/DataRoutes';
import FeedbackRoutes from './server/src/routes/FeedbackRoutes';

JobsController.jobsHandler();

config.config();
const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

const port = process.env.PORT || 8000;

app.use('/api/data', DataRoutes);
app.use('/api/house', HouseRoutes);
app.use('/api/profile', ProfileRoutes);
app.use('/api/sensor', SensorRoutes);
app.use('/api/events', ServerEventRoutes);
app.use('/api/alert', AlertRoutes);
app.use('/api/feedback', FeedbackRoutes);

// when a random route is inputed
app.get('*', (request, response) =>
  response.status(200).send({
    message: 'Welcome to this API.',
  }),
);

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});

export default app;
